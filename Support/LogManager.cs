﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;

namespace WeChatServe.Support
{
    /// <summary>
    /// 对外提供一个单例，允许借此向本地文件中输出内容
    /// </summary>
    public class LogManager
    {
        private static readonly LogManager singleton = new LogManager();

        private LogManager()
        {
            //如果Log文件夹不存在，则创建。
            if (Directory.Exists(HttpContext.Current.Server.MapPath(Universal.logFolder)) == false)
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(Universal.logFolder));
            }

            //不管output文件存不存在，新建一个覆盖
            FileStream createStram = new FileStream(HttpContext.Current.Server.MapPath(Universal.logFolder + "\\" + Universal.outputFile), FileMode.Create);
            createStram.Close();

        }

        public static LogManager getLogManager()
        {
            return singleton;
        }

        /// <summary>
        /// D.O
        /// 输出信息到文件，每次编译后重新执行都会导致上次写入的内容丢失
        /// 大量的IO操作将会减慢速度
        /// </summary>
        /// <param name="content">将要写入的文本内容</param>
        public void writeToOutput(string content)
        {
            #if DEBUG
            FileStream outputStream = new FileStream(HttpContext.Current.Server.MapPath(Universal.logFolder + "\\" + Universal.outputFile), FileMode.Append);
            StreamWriter outputWriter = new StreamWriter(outputStream, Encoding.UTF8);
            outputWriter.WriteLine(content);
            outputWriter.Close();
            outputStream.Close();
            #endif
        }
    }
}