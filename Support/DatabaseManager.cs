﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace WeChatServe.Support
{
    public class DatabaseManager
    {
        private static readonly DatabaseManager singleton = new DatabaseManager();
        public MySqlConnection conn;

        private DatabaseManager()
        {
            string connStr = "server=" + Universal.databaseUrl
                 + ";user=" + Universal.databaseUsername
                 + ";database=" + Universal.databaseName
                 + ";port=" + Universal.databasePort
                 + ";password=" + Universal.databasePassword
                 + ";";
            conn = new MySqlConnection(connStr);
            try
            {
                conn.Open();
            }
            catch(Exception ex)
            {
                LogManager.getLogManager().writeToOutput("DB openConn:" + ex.Message);
            }
        }

        public static DatabaseManager getDBManager()
        {
            return singleton;
        }

        public void excuteNonQuery(string sqlStr)
        {
            try
            {
                MySqlCommand cmd = new MySqlCommand(sqlStr, conn);
                cmd.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                conn.Close();
                conn.Open();
                LogManager.getLogManager().writeToOutput("DB excuteNoneQuery:" + ex.Message);
            }
        }

        public int? excuteNumQuery(string sqlStr)
        {
            try
            {
                MySqlCommand cmd = new MySqlCommand(sqlStr, conn);
                MySqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                string result = reader[0].ToString();
                reader.Close();
                //LogManager.getLogManager().writeToOutput("reader of sql num = " + result);
                return int.Parse(result);
            }
            catch(Exception ex)
            {
                LogManager.getLogManager().writeToOutput("DB excuteNumQuery" + ex.Message);
                conn.Close();
                conn.Open();
                return null;
            }
        }

        public MySqlDataReader excuteQuery(string sqlStr)
        {
            try
            {
                MySqlCommand cmd = new MySqlCommand(sqlStr, conn);
                return cmd.ExecuteReader();
            }
            catch(Exception ex)
            {
                LogManager.getLogManager().writeToOutput("DB excuteQuery" + ex.Message);
                conn.Close();
                conn.Open();
                return null;
            }
        }

        public void reopenConn()
        {
            conn.Close();
            conn.Open();
        }
    }
}