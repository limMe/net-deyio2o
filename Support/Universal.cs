﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace WeChatServe.Support
{
    /// <summary>
    /// E.D
    /// 提供全局变量，多是环境依赖的应用程序设置
    /// </summary>
    public static class Universal
    {

        #region debug
        //数据库配置
        public static string databaseUrl = "deyicard.mysql.rds.aliyuncs.com";
        public static string databaseUsername = "testor";
        public static string databaseName = "dy_test";
        #endregion

        //appid appsecret写入了web.config,需要在那里更改
        #region release
        /*
        public static string databaseUrl = ;
        public static string databaseUsername = "deyi_o2o";
        public static string databaseName = "dy_release";
        */
        #endregion

        #region no-change

        //数据库配置
        public static string databasePort = "3306";
        public static string databasePassword = "19941201";

        public const int maxCardNum = 99999999;
        public const int maxMoney100 = 500000;

        //数据库字段构构建


        #region COUNT_sql
        public static string formWhetherCardExists(string card_num)
        {
            return "select COUNT(*) from `users` where card_num = " + card_num + ";";
        }
        
        public static string formWhetherTokenAvalible(string token)
        {
            return "select COUNT(*) from `hour_token` where token = " + token + ";";
        }

        public static string formWhetherWechatHasCard(string open_id)
        {
            return "select COUNT(*) from `users` where bundling_open_id = '" + open_id + "' ;";
        }

        public static string formWhetherCardNeedVerify(string card_num)
        {
            return "select COUNT(*) from `unverified_cards` where card_num = " + card_num + ";";
        }

        public static string formWhetherAdminIdRight(string openId)
        {
            return "select COUNT(*) from `admins` where open_id = '" + openId + "';";
        }

        #endregion
   
        #region INSERT_sql
        public static string formInsertUnverifiedCard(string card_num, string intro_saler_id, string verify_num, string date)
        {
            return "insert into `unverified_cards` (`card_num`,`intro_saler_id`,`verify_num`,`date`)"
                     + "values(" + card_num + "," +intro_saler_id + "," + verify_num + "," + date +");";
        }

        public static string formInsertNewDeal(string card_num, string saler_id, string money100, string date, string time)
        {
            return "insert into `deals` (`saler_id`,`user_card`,`amount`,`date`,`time`)"
                     + "values("+saler_id+" ,"+card_num+","+money100+","+date+","+time+");";
        }

        public static string formInsertNewToken(string hour, string minute, string token)
        {
            return "insert into `hour_token` (`minute`,`hour`,`token`)"
                + "values(" + minute + "," + hour + ",'" + token + "');";
        }

        public static string formInsertNewSaler(string saler_name, string saler_location, string saler_district,string saler_type,string activate_code)
        {
            return "insert into `salers` (`saler_name`,`saler_location`,`saler_district`,`saler_type`,`saler_activate_code`)"
                +"values('"+saler_name+"','"+saler_location+"',"+saler_district+","+saler_type+","+activate_code+");";
        }

        public static string formInsertNewWeChatUser(string open_id)
        {
            return "insert into `users_on_wechat` (`open_id`,`init_date`)"
                     + "values('" + open_id + "'," + DateTime.Now.ToString("yyyyMMdd") + ";";
        }

        public static string formInsertNewCardUser(string card_num, string open_id, string intro_saler, string intro_user)
        {
            return "insert into `users` (`card_num`,`bundling_open_id`,`intro_saler_id`,`intro_user_card`) "
                     + "values(" + card_num + ",'" + open_id + "'," + intro_saler + "," + intro_user + ");";
        }

        #endregion

        #region DELETE_sql
        public static string formDeleteOldToken(string hour, string minute)
        {
            if(hour=="00"||hour=="0")
            {
                return "DELETE FROM `hour_token` WHERE hour < 24 AND minute < " + minute+";";
            }
            return "DELETE FROM `hour_token` WHERE (hour < " + hour + " AND minute < " + minute + " ) OR hour > " + hour + ";";
        }

        public static string formDeleteWeChatUser(string open_id)
        {
            return "DELETE FROM `users_on_wechat` where open_id = '" + open_id + "';";
        }

        public static string formDeleteCardNeedVerify(string car_num)
        {
            return "DELETE FROM `unverified_cards` WHERE card_num = " + car_num + ";";
        }

        #endregion

        #region QUERY_sql
        /// <summary>
        /// select `deal_id`,`saler_id`, `user_card`,`amount`,`date` from `deals`
        /// </summary>
        public static string  formSalerRecordsQuery(string saler_id, string begin_time, string end_time)
        {
            return "select `deal_id`,`saler_id`, `user_card`,`amount`,`date`,`time` from `deals`"
                     + "where date >=" + begin_time + " AND date <= "+ end_time +" AND saler_id ="+saler_id+";";
        }

        /// <summary>
        /// select `deal_id`,`saler_id`, `user_card`,`amount`,`date` from `deals`
        /// </summary>
        public static string formCustomerRecordsQuery(string user_card, string begin_time, string end_time)
        {
            return "select `deal_id`,`saler_id`, `user_card`,`amount`,`date`,`time` from `deals`"
                     + "where date >=" + begin_time + " AND date <= " + end_time + " AND user_card =" + user_card + ";";
        }

        public static string formGetNonceToken(string hour, string minute)
        {
            return "select `token` from `hour_token` where hour =" + hour + " AND minute=" + minute + ";";
        }

        public static string formGetVerifyNumQuery(string card_num)
        {
            return "select  `verify_num` from `unverified_cards` where `card_num` ="+card_num +";";
        }

        public static string formGetVerifyInfoQuery(string card_num)
        {
            return "select  `verify_num`,`intro_saler_id`,`intro_user_card` from `unverified_cards` where `card_num` =" + card_num + ";";
        }

        /// <summary>
        /// select `username`, `bundling_open_id`,`user_type`,`intro_saler_id`, `points`
        /// </summary>
        public static string formGetUserInfoQuery(string card_num)
        {
            return "select `username`, `bundling_open_id`,`user_type`,`intro_saler_id`, `points` from `users` where card_num = '" + card_num + "';";
        }

        /// <summary>
        /// select  `saler_name`, `saler_open_id` from `salers`
        /// </summary>
        public static string formGetSalerInfoQuery(string saler_id)
        {
            //居然对数字采用单引号也不会错诶
            return "select  `saler_name`, `saler_open_id` from `salers` where saler_id ='" + saler_id + "';";
        }

        /// <summary>
        /// select `saler_id`,`saler_name`,`saler_open_id`,`saler_location`,`saler_district`,`saler_type`,`saler_activate_code`
        /// </summary>
        /// <param name="saler_id"></param>
        /// <returns></returns>
        public static string formGetFullSalerInfoQuery(string saler_id)
        {
            return "select `saler_id`,`saler_name`,`saler_open_id`,`saler_location`,`saler_district`,`saler_type`,`saler_activate_code`"
                +"from `salers` where saler_id = '" + saler_id + "';";
        }

        public static string formGetAllSalerInfoQuery()
        {
            return "select `saler_id`,`saler_name`,`saler_open_id`,`saler_location`,`saler_district`,`saler_type`,`saler_activate_code`"
                + " from `salers`;";
        }

        public static string formGetCardNumByOpenId(string open_id)
        {
            return "select `card_num` from `users` where bundling_open_id = '"+ open_id+"';"; 
        }

        public static string formGetFullSalerOpenId(string saler_id)
        {
            return "select `saler_open_id` from `salers` where saler_id =" + saler_id + ";";
        }

        public static string formGetSalerIdByOpenId(string openId)
        {
            return "select `saler_id` from `salers` where saler_open_id LIKE '%" + openId + "%';";
        }

        #endregion

        #region UPDATE_sql
        public static string formUpdateUserPoints(string card_num, string money100)
        {
            return "UPDATE `users` SET points = points + " + money100 + " WHERE card_num = " + card_num+";";
        }

        public static string formUpdateSalerOpenId(string saler_id, string saler_open_id)
        {
            return "UPDATE `salers` SET saler_open_id = '" + saler_open_id + "' WHERE saler_id = " + saler_id + ";";
        }
        #endregion

        //日志输出
        public static string logFolder = "log";
        public static string outputFile = "output.log";

        //数字处理
        /// <summary>
        /// 将倍乘消掉小数点的数字转换为可展示的金钱数额，后面会加元
        /// </summary>
        /// <param name="money100">倍乘消掉小数点的数字</param>
        /// <returns>可展示的金钱数额，后面会加元</returns>
        public static string getDisplayableMoney(int money100)
        {
            int afterDot = money100 % 100;
            int beforeDot = money100 / 100;
            return beforeDot.ToString() + "." + afterDot.ToString() + "元";
        }

        public static int? toMoney100(string moneyWithDot)
        {
            try
            {
                if (moneyWithDot.IndexOf('.') > -1
                    || moneyWithDot.IndexOf('。') > -1)
                {
                    string[] twoparts = moneyWithDot.Split('.', '。');
                    if (twoparts[1].Length == 1)
                    {
                        return Int32.Parse(twoparts[0] + twoparts[1] + "0");
                    }
                    return Int32.Parse(twoparts[0] + twoparts[1].Substring(0, 2));
                }
                else
                    return Int32.Parse(moneyWithDot + "00");
            }
            catch
            {
                return null;
            }
        }

        //E.D M.S 这里的key用于本地配置的加密和网络传输的加密，小心！
        private static string initKey = "justASimpleKey";
        private static byte[] DESKey = Encoding.UTF8.GetBytes(initKey.Substring(0, 8));
        private static byte[] DESIV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };
        public static string encryptByMD5(string str)
        {
            MD5 md5 = MD5.Create();
            byte[] encodedBytes = md5.ComputeHash(Encoding.UTF8.GetBytes(str));
            return Encoding.UTF8.GetString(encodedBytes);
        }

        public static string encryptByDES(string encryptString)
        {
            try
            {
                byte[] rgbKey = DESKey;
                byte[] rgbIV = DESIV;
                byte[] inputByteArray = Encoding.UTF8.GetBytes(encryptString);
                DESCryptoServiceProvider dCSP = new DESCryptoServiceProvider();
                MemoryStream mStream = new MemoryStream();
                CryptoStream cStream = new CryptoStream(mStream, dCSP.CreateEncryptor(rgbKey, rgbIV), CryptoStreamMode.Write);
                cStream.Write(inputByteArray, 0, inputByteArray.Length);
                cStream.FlushFinalBlock();
                return Convert.ToBase64String(mStream.ToArray());
            }
            catch
            {
                return encryptString;
            }
        }

        public static string decryptByDES(string decryptString)
        {
            try
            {
                byte[] rgbKey = DESKey;
                byte[] rgbIV = DESIV;
                byte[] inputByteArray = Convert.FromBase64String(decryptString);
                DESCryptoServiceProvider DCSP = new DESCryptoServiceProvider();
                MemoryStream mStream = new MemoryStream();
                CryptoStream cStream = new CryptoStream(mStream, DCSP.CreateDecryptor(rgbKey, rgbIV), CryptoStreamMode.Write);
                cStream.Write(inputByteArray, 0, inputByteArray.Length);
                cStream.FlushFinalBlock();
                return Encoding.UTF8.GetString(mStream.ToArray());
            }
            catch
            {
                return decryptString;
            }
        }

        public static bool verifyEncrpt(string origin, string encrpted)
        {
            if(Universal.encryptByDES(origin) == encrpted)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string getCurrentUnixTicks()
        {
            long ticks = (DateTime.Now.ToUniversalTime().Ticks - 621355968000000000) / 10000000;
            return ticks.ToString();
        }
        #endregion

    }

    public enum User_type
    {
        Odinary = 0,
        Agent = 1,
    };
}