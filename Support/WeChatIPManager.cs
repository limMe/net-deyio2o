﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WX.Api;
using WX.Framework;
using WX.Model.ApiRequests;
using WX.Model.ApiResponses;

namespace WeChatServe.Support
{
    public class WeChatIPManager
    {
        private static readonly WeChatIPManager singleton = new WeChatIPManager();

        private WeChatIPManager()
        {
            refreshList();
        }

        public static WeChatIPManager getIPManager()
        {
            return singleton;
        }

        public IEnumerable<string> IPList;

        public void refreshList()
        {
            var ipListRequest = new GetcallbackipRequest()
            {
                AccessToken = ApiAccessTokenManager.Instance.GetCurrentToken(),
            };
            IApiClient ipListClient = new DefaultApiClient();
            var ipListResponse = ipListClient.Execute(ipListRequest);
            if(ipListResponse.IsError || ipListResponse.IPList == null)
            {

            }
            else
            {
                IPList = ipListResponse.IPList;
            }
        }
    }
}