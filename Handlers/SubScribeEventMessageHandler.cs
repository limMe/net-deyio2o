﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using WX.Framework;
using WX.Model;
using WX.Api;
using WX.Model.ApiRequests;
using WeChatServe.Support;
using WeChatServe.BLL;

namespace WeChatServe.Handlers
{

    public class SubScribeEventMessageHandler : IMessageHandler
    {
        
        /// <summary>
        /// 将新关注用户的OPEN_ID写入数据库，作为主键
        /// </summary>
        /// <param name="msg"></param>
        public SubScribeEventMessageHandler(MiddleMessage msg)
        {
            
            string newUserId = msg.Element.Element("FromUserName").Value;
            DatabaseManager.getDBManager().excuteNonQuery(Universal.formInsertNewWeChatUser(newUserId));

        }

        public ResponseMessage HandlerRequestMessage(MiddleMessage msg)
        {
            return NewsMsgFormer.formWelcomeNewsMsg(msg.RequestMessage);
        }
    }
}