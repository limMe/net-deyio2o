﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using WX.Framework;
using WX.Model;

namespace WeChatServe.Handlers
{
    public class DefaultMessageHandler : IMessageHandler
    {
        private static string s_defaultMsg = "同学您好，小得会尽快处理您的信息，请您耐心等待一下。";

        public ResponseMessage HandlerRequestMessage(MiddleMessage msg)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["isAutoReply"] == "true")
            {
                return new ResponseTextMessage(msg.RequestMessage)
                {
                    CreateTime = DateTime.Now.Ticks,
                    Content = s_defaultMsg
                };
            }
            return new ResponseTextMessage(msg.RequestMessage)
            {
                CreateTime = DateTime.Now.Ticks,
                
            };
        }
    }
}