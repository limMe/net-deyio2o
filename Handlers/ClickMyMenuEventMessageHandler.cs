﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using WX.Framework;
using WX.Model;
using WX.Api;
using WX.Model.ApiRequests;
using WeChatServe.Support;
using WeChatServe.BLL;

namespace WeChatServe.Handlers
{
    class ClickMyMenuEventMessageHandler : IMessageHandler
    {
        
        public ClickMyMenuEventMessageHandler(MiddleMessage msg)
        {
        }

        public ResponseMessage HandlerRequestMessage(MiddleMessage msg)
        {     
            try
            {
                switch(msg.Element.Element("EventKey").Value)
                {
                    case "ME_ACTIVATE":
                        return NewsMsgFormer.formActiveCardInfo(msg.RequestMessage);
                    case "ME_QUERY":
                        return NewsMsgFormer.formCustomerRecordsNewsMsg(msg.RequestMessage);
                    case "ME_SALER":
                        return NewsMsgFormer.formSalerEntryNewsMsg(msg.RequestMessage);
                    case "NEAR_FOOD":
                        return NewsMsgFormer.formFoodNewsMsg(msg.RequestMessage);
                    case "NEAR_PLAY":
                        return NewsMsgFormer.formPlayNewsMsg(msg.RequestMessage);
                    case "NEAR_SERVICE":
                        return NewsMsgFormer.formServiceNewsMsg(msg.RequestMessage);
                    case "DEYI_SCHOOL":
                        return NewsMsgFormer.formSchoolNewsMsg(msg.RequestMessage);
                    case "ACTIVITY_NOW":
                        return NewsMsgFormer.formActivityNowNewsMsg(msg.RequestMessage);
                    case "ACTIVITY_FAQ":
                        return NewsMsgFormer.formFAQNewsMsg(msg.RequestMessage);
                    default:
                        return NewsMsgFormer.form404NewsMsg(msg.RequestMessage);
                }
            }
            catch(Exception ex)
            {
                LogManager.getLogManager().writeToOutput(ex.Message);
                return new ResponseTextMessage(msg.RequestMessage)
                {
                    CreateTime = DateTime.Now.Ticks,
                    Content = ex.Message,
                };
            }
            finally
            {

            }
        }


    }
}
