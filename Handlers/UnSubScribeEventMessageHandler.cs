﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using WX.Framework;
using WX.Model;
using WX.Api;
using WX.Model.ApiRequests;
using WeChatServe.Support;

namespace WeChatServe.Handlers
{
    public class UnSubScribeEventMessageHandler : IMessageHandler
    {
        /// <summary>
        /// 由于open_id是主键，取消关注不能将卡片与微信号解绑
        /// </summary>
        /// <param name="msg"></param>
        public UnSubScribeEventMessageHandler(MiddleMessage msg)
        {
            string userId = msg.Element.Element("FromUserName").Value;
            DatabaseManager.getDBManager().excuteNonQuery(Universal.formDeleteWeChatUser(userId));
        }

        public ResponseMessage HandlerRequestMessage(MiddleMessage msg)
        {
            return new ResponseTextMessage(msg.RequestMessage)
            {
                CreateTime = DateTime.Now.Ticks,
                Content = ""
            };
        }
    }
}