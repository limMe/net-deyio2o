﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using WX.Framework;
using WX.Model;


namespace WeChatServe.Handlers
{
    public class FunnyTextMessageHandler : IMessageHandler
    {
        private static string s_Msg =
           "天呐，你竟然认识人大校草钟典";
        public ResponseMessage HandlerRequestMessage(MiddleMessage msg)
        {

            return new ResponseTextMessage(msg.RequestMessage)
            {
                CreateTime = DateTime.Now.Ticks,
                Content = s_Msg
            };
        }
    }
}