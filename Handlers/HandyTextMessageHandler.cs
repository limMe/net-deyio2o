﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using WX.Framework;
using WX.Model;

namespace WeChatServe.Handlers
{
    /// <summary>
    /// 提供便捷的文本回复方法，构造函数输入的字符串将被原样发送给用户
    /// </summary>
    public class HandyTextMessageHandler:TextMessageHandler
    {
        /// <summary>
        /// 提供便捷的文本回复方法，构造函数输入的字符串将被原样发送给用户
        /// </summary>
        /// <param name="stringToUser">发送给用户的字符</param>
        public HandyTextMessageHandler(string stringToUser)
            : base(stringToUser)
        {

        }
    }
}