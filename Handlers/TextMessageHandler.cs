﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using WX.Framework;
using WX.Model;
using WeChatServe.BLL;
using WeChatServe.Support;
using WeChatServe.InteractModel;

namespace WeChatServe.Handlers
{
    /// <summary>
    /// 若干返回文本的handler由此派生
    /// </summary>
    public class TextMessageHandler : IMessageHandler
    {
        private string Message { get; set; }

        public TextMessageHandler(string msg)
        {
            Message = msg;
        }

        public ResponseMessage HandlerRequestMessage(MiddleMessage msg)
        {
            //M.D 建议用正则
            if(Message.IndexOf("激活") > -1)
            {
                string msgToReturn = "";
                try
                {
                    string[] infos = Message.Split(',', '，', ' ');
                    ActiveCardModel activeToDo = new ActiveCardModel();
                    activeToDo.cardId = int.Parse(infos[1]);
                    activeToDo.activecode = int.Parse(infos[2]);
                    activeToDo.openid = msg.RequestMessage.FromUserName;
                    BussinessActions.verifyCard(activeToDo);
                    msgToReturn = "成功激活您的卡号！现在您即可使用完整的得益卡服务。请勿轻易取消关注得益卡官方微信哦!";
                }
                catch (Exception ex)
                {
                    msgToReturn = ex.Message;
                }

                return new ResponseTextMessage(msg.RequestMessage)
                {
                    CreateTime = DateTime.Now.Ticks,
                    Content = msgToReturn
                };
            }

            else if(Message.IndexOf("积分") > -1)
            {
                string msgToReturn = "";
                try
                {
                    string[] infos = Message.Split(',', '，', ' ');
                    string saler_id = VerifyActions.verifySalerOpenId(msg.RequestMessage.FromUserName);
                    if(saler_id == null)
                    {
                        throw new Exception("您尝试新增一笔消费，但是您并未取得商家认证！");
                    }
                    var dealToAdd = new NewDealModel();
                    int card;
                    if(!int.TryParse(infos[1], out card))
                    {
                        throw new Exception("用户卡号格式不对！");
                    }
                    dealToAdd.customerCard = card;

                    if(Universal.toMoney100(infos[2]) == null)
                    {
                        throw new Exception("消费金额格式不对！");
                    }
                    dealToAdd.money100 = (int)Universal.toMoney100(infos[2]);
                    dealToAdd.salerId = int.Parse(saler_id);
                    BussinessActions.addNewDeal(dealToAdd);

                    msgToReturn = "成功新增交易。已给用户发送相关交易提醒信息。";
                }
                catch (Exception ex)
                {
                    msgToReturn = ex.Message;
                }
                
                return new ResponseTextMessage(msg.RequestMessage)
                {
                    CreateTime = DateTime.Now.Ticks,
                    Content = msgToReturn
                };
            }

            else if (Message.IndexOf("发卡") > -1)
            {
                string msgToReturn = "";
                try
                {
                    string[] infos = Message.Split(',', '，', ' ');
                    string saler_id = VerifyActions.verifySalerOpenId(msg.RequestMessage.FromUserName);
                    if (saler_id == null)
                    {
                        throw new Exception("您尝试新发一张卡片，但是您并未取得商家认证！");
                    }

                    var newCard = new NewCardModel();
                    newCard.salerId = int.Parse(saler_id);
                    
                    int card;
                    if (!int.TryParse(infos[1], out card))
                    {
                        throw new Exception("用户卡号格式不对！");
                    }
                    newCard.customerCard = card;
                    string result = BussinessActions.publishNewCard(newCard);
                    var publishResult = Newtonsoft.Json.JsonConvert.DeserializeObject<AddCardResultModel>(result);

                    if(publishResult.status == false)
                    {
                        throw new Exception(publishResult.errorMsg);
                    }

                    msgToReturn = "发卡已成功。该卡的验证码为："+publishResult.verifyNum;
                }
                catch (Exception ex)
                {
                    msgToReturn = ex.Message;
                }

                return new ResponseTextMessage(msg.RequestMessage)
                {
                    CreateTime = DateTime.Now.Ticks,
                    Content = msgToReturn
                };
            }

            else if(Message.IndexOf("绑定商家") > -1)
            {
                string msgToReturn = "";
                try
                {
                    string[] infos = Message.Split(',', '，', ' ');
                    BussinessActions.bandleSalerWechat(infos[1],infos[2],msg.RequestMessage.FromUserName);
                    msgToReturn = "您已经成功认证为商家。";
                }
                catch (Exception ex)
                {
                    msgToReturn = ex.Message;
                }

                return new ResponseTextMessage(msg.RequestMessage)
                {
                    CreateTime = DateTime.Now.Ticks,
                    Content = msgToReturn
                };
            }
            else if(Message == "麻利麻利哄")
            {
                try
                {
                    if(!VerifyActions.verifyAdminOpenId(msg.RequestMessage.FromUserName))
                    {
                        throw new Exception("您的法力不足，无法施放这个咒语。" );
                    }
                    return NewsMsgFormer.formAdminLoginNewsMsg(msg.RequestMessage);
                }
                catch(Exception ex)
                {
                    return new ResponseTextMessage(msg.RequestMessage)
                    {
                        CreateTime = DateTime.Now.Ticks,
                        Content = ex.Message
                    };
                }
            }
            else if (Message == "波若波罗密")
            {
                try
                {
                    if (!VerifyActions.verifyAdminOpenId(msg.RequestMessage.FromUserName))
                    {
                        throw new Exception("您的法力不足，无法施放这个咒语。");
                    }
                    return NewsMsgFormer.formAdminQueryNewsMsg(msg.RequestMessage);
                }
                catch (Exception ex)
                {
                    return new ResponseTextMessage(msg.RequestMessage)
                    {
                        CreateTime = DateTime.Now.Ticks,
                        Content = ex.Message
                    };
                }
            }
			else if (Message == "叶翘力")
			{  
				return new ResponseTextMessage(msg.RequestMessage)
				{
						CreateTime = DateTime.Now.Ticks,
                        Content = "我太tm帅了"
				};
			}
            else
            {
                if (System.Configuration.ConfigurationManager.AppSettings["isAutoReply"] == "true")
                {
                    return new ResponseTextMessage(msg.RequestMessage)
                    {
                        CreateTime = DateTime.Now.Ticks,
                        Content = "同学您好，小得会尽快处理您的信息，请您耐心等待一下。"
                    };
                }
                return new ResponseTextMessage(msg.RequestMessage)
                {
                    CreateTime = DateTime.Now.Ticks,
                    Content = ""
                };
            }
        }
    }
}