﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using WeChatServe.Roles;
using WeChatServe.Support;
using WX.Framework;
using WX.Model;
using WX.Api;
using WX.Model.ApiRequests;
using WX.Model.ApiResponses;

namespace WeChatServe
{
    /// <summary>
    /// 处于Debug状态时可以用这个页面的函数来做微信设置。
    /// </summary>
    public partial class SetUpWeChat : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #if DEBUG
            createNewBtns();
            #endif
        }

        private void createNewBtns()
        {
            MenuCreateRequest mcRequest = new MenuCreateRequest()
            {
                AccessToken = ApiAccessTokenManager.Instance.GetCurrentToken(),
                Buttons = new List<ClickButton>()
                {
                    new ClickButton()
                    {
                        Name = System.Configuration.ConfigurationManager.AppSettings["menuBtn1Name"],
                        Type = ClickButtonType.click,
                        Key = System.Configuration.ConfigurationManager.AppSettings["menuBtn1Key"],
                    },
                    new ClickButton()
                    {
                        Name = System.Configuration.ConfigurationManager.AppSettings["menuBtn2Name"],
                        Type = ClickButtonType.click,
                        Key = System.Configuration.ConfigurationManager.AppSettings["menuBtn2Key"],
                    },
                },
            };

            IApiClient mcClient = new DefaultApiClient();
            MenuCreateResponse mcResponse = mcClient.Execute(mcRequest);
            if(mcResponse.IsError)
            {
                LogManager.getLogManager().writeToOutput(mcResponse.ErrorMessage);
            }
        }

    }
}