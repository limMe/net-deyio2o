﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using WeChatServe.Roles;
using WeChatServe.Support;
using WX.Framework;
using WX.Model;

namespace WeChatServe
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            try
            {
                //微信服务器一直把用户发过来的消息post过来
                if (Request.HttpMethod == "POST")
                {
                    var reader = XmlReader.Create(Request.InputStream);
                    var doc = XDocument.Load(reader);
                    
                    var xml = doc.Element("xml");
                    var msg = new MiddleMessage(xml);

                    //根据预先填写的TOKEN识别微信身份
                    var signature = Request.QueryString["signature"];
                    var timestamp = Request.QueryString["timestamp"];
                    var nonce = Request.QueryString["nonce"];
                    var token = System.Configuration.ConfigurationManager.AppSettings["wechatToken"];
                    //字典序排列字符串
                    List<string> list = new List<string>();
                    list.Add(token);
                    list.Add(timestamp);
                    list.Add(nonce);
                    list.Sort();
                    string res = string.Join("", list.ToArray());
                    //sha1加密
                    Byte[] data1ToHash = Encoding.ASCII.GetBytes(res);
                    byte[] hashvalue1 = ((HashAlgorithm)CryptoConfig.CreateFromName("SHA1")).ComputeHash(data1ToHash);
                    StringBuilder sb = new StringBuilder();
                    foreach (byte b in hashvalue1)
                    {
                        sb.Append(b.ToString("x2"));
                    }
                    string s = BitConverter.ToString(hashvalue1).Replace("-", string.Empty).ToLower();
                    //异常处理
                    if (signature != sb.ToString())
                    {
                        throw new Exception("消息请求来自非微信");
                    }


                    //把inputstream转换成xelement后，直接交给WebMessageRole来处理吧
                    var responseMessage = new WebMessageRole()
                        .MessageRole(msg)
                        .HandlerRequestMessage(msg);

                    if (responseMessage != null)
                    {
                        Response.Write(responseMessage.Serializable());
                    }
                }

                else if (Request.HttpMethod == "GET") //微信服务器在首次验证时，需要进行一些验证，但。。。。
                {
                    //我仅需返回给他echostr中的值，就为验证成功，可能微信觉得这些安全策略是为了保障我的服务器，要不要随你吧
                    Response.Write(Request["echostr"].ToString());
                }
            }
            catch (Exception ex)
            {
                LogManager.getLogManager().writeToOutput(ex.Message);
            }
        }
    }
}