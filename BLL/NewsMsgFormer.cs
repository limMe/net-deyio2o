﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using WX.Framework;
using WX.Model;
using WX.Api;
using WX.Model.ApiRequests;
using WeChatServe.Support;

namespace WeChatServe.BLL
{
    public static class NewsMsgFormer 
    {
        static NewsMsgFormer()
        {

        }

        public static ResponseMessage formWelcomeNewsMsg(RequestMessage msg)
        {
            return new ResponseTextMessage(msg)
                {
                    CreateTime = DateTime.Now.Ticks,
                    Content = System.Configuration.ConfigurationManager.AppSettings["welcomeInfo"] + " "
                };
        }

        public static ResponseMessage formActiveCardInfo(RequestMessage msg)
        {
            var welcomeArticle = new ArticleMessage()
            {
                Title = "戳我激活您的得益卡",
                PicUrl = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/Img/activeCard.jpg",
                Url = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/activeCard.html?openid=" + msg.FromUserName
                    + "&tick=" + Universal.getCurrentUnixTicks() + "&isViewOnly=false&func=FUNC_ACTIVECARD",
            };

            var messageToReturn = new ResponseNewsMessage(msg)
            {
                CreateTime = DateTime.Now.Ticks,
                ArticleCount = 1,
                Articles = new List<ArticleMessage>(),
            };
            messageToReturn.Articles.Add(welcomeArticle);
            return messageToReturn;
        }

        public static ResponseMessage formActivityNowNewsMsg(RequestMessage msg)
        {
            var welcomeArticle = new ArticleMessage()
            {
                Title = "最近好玩实惠的活动都在得益卡",
                PicUrl = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/Img/wechat_cover_activiy.jpg",
                Url = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/activityNow.html?openid=" + msg.FromUserName + "&tick=" + DateTime.Now.Ticks.ToString() + "&isViewOnly=true",
            };

            var messageToReturn = new ResponseNewsMessage(msg)
            {
                CreateTime = DateTime.Now.Ticks,
                ArticleCount = 1,
                Articles = new List<ArticleMessage>(),
            };
            messageToReturn.Articles.Add(welcomeArticle);
            return messageToReturn;
        }

        public static ResponseMessage formFAQNewsMsg(RequestMessage msg)
        {
            var welcomeArticle = new ArticleMessage()
            {
                Title = "疑难杂症？小得回应你",
                PicUrl = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/Img/wechat_cover_faq.jpg",
                Url = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/faq.html?openid=" + msg.FromUserName + "&tick=" + DateTime.Now.Ticks.ToString() + "&isViewOnly=true",
            };

            var messageToReturn = new ResponseNewsMessage(msg)
            {
                CreateTime = DateTime.Now.Ticks,
                ArticleCount = 1,
                Articles = new List<ArticleMessage>(),
            };
            messageToReturn.Articles.Add(welcomeArticle);
            return messageToReturn;
        }

        public static ResponseMessage formFoodNewsMsg(RequestMessage msg)
        {
            var welcomeArticle = new ArticleMessage()
            {
                Title = "附近有什么好吃的？",
                PicUrl = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/Img/wechat_cover_food.jpg",
                Url = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/foodNearby.html?openid=" + msg.FromUserName + "&tick=" + DateTime.Now.Ticks.ToString() + "&isViewOnly=true",
            };

            var messageToReturn = new ResponseNewsMessage(msg)
            {
                CreateTime = DateTime.Now.Ticks,
                ArticleCount = 1,
                Articles = new List<ArticleMessage>(),
            };
            messageToReturn.Articles.Add(welcomeArticle);
            return messageToReturn;
        }

        public static ResponseMessage formPlayNewsMsg(RequestMessage msg)
        {
            var welcomeArticle = new ArticleMessage()
            {
                Title = "附近有什么好玩的？",
                PicUrl = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/Img/wechat_cover_play.jpg",
                Url = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/playNearby.html?openid=" + msg.FromUserName + "&tick=" + DateTime.Now.Ticks.ToString() + "&isViewOnly=true",
            };

            var messageToReturn = new ResponseNewsMessage(msg)
            {
                CreateTime = DateTime.Now.Ticks,
                ArticleCount = 1,
                Articles = new List<ArticleMessage>(),
            };
            messageToReturn.Articles.Add(welcomeArticle);
            return messageToReturn;
        }


        public static ResponseMessage formServiceNewsMsg(RequestMessage msg)
        {
            var welcomeArticle = new ArticleMessage()
            {
                Title = "美容美发美甲...我这么多需求上哪满足？",
                PicUrl = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/Img/wechat_cover_service.jpg",
                Url = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/serviceNearby.html?openid=" + msg.FromUserName + "&tick=" + DateTime.Now.Ticks.ToString() + "&isViewOnly=true",
            };

            var messageToReturn = new ResponseNewsMessage(msg)
            {
                CreateTime = DateTime.Now.Ticks,
                ArticleCount = 1,
                Articles = new List<ArticleMessage>(),
            };
            messageToReturn.Articles.Add(welcomeArticle);
            return messageToReturn;
        }

        public static ResponseMessage formSchoolNewsMsg(RequestMessage msg)
        {
            var welcomeArticle = new ArticleMessage()
            {
                Title = "校园内的各种实惠活动！",
                PicUrl = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/Img/wechat_cover_school.jpg",
                Url = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/deyiSchool.html?openid=" + msg.FromUserName + "&tick=" + DateTime.Now.Ticks.ToString() + "&isViewOnly=true",
            };

            var messageToReturn = new ResponseNewsMessage(msg)
            {
                CreateTime = DateTime.Now.Ticks,
                ArticleCount = 1,
                Articles = new List<ArticleMessage>(),
            };
            messageToReturn.Articles.Add(welcomeArticle);
            return messageToReturn;
        }

        public static ResponseMessage form404NewsMsg(RequestMessage msg)
        {
            var welcomeArticle = new ArticleMessage()
            {
                Title = "您所希望使用的功能尚在开发中。",
                PicUrl = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/Img/wechat_cover_404.JPG",
                Url = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/underDev.html?openid=" + msg.FromUserName + "&tick=" + DateTime.Now.Ticks.ToString() + "&isViewOnly=true",
            };

            var messageToReturn = new ResponseNewsMessage(msg)
            {
                CreateTime = DateTime.Now.Ticks,
                ArticleCount = 1,
                Articles = new List<ArticleMessage>(),
            };
            messageToReturn.Articles.Add(welcomeArticle);
            return messageToReturn;
        }

        public static ResponseMessage formCustomerRecordsNewsMsg(RequestMessage msg)
        {
            string userCard = null;
            userCard = VerifyActions.getUserCardByOpenId(msg.FromUserName);
            if(userCard==null)
            {
                return new ResponseTextMessage(msg)
                {
                    CreateTime = DateTime.Now.Ticks,
                    Content = "您的微信并没有绑定得益卡，无法查询您的消费记录。赶快申请得益卡享受更多服务吧！"
                };
            }
            var welcomeArticle = new ArticleMessage()
            {
                Title = "快来查询您的消费账单！",
                PicUrl = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/Img/wechat_cover_customerDeals.png",
                Url = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/customer.html?openid=" + msg.FromUserName 
                    + "&tick="+Universal.getCurrentUnixTicks()+"&isViewOnly=false&func=FUNC_QUERY_CUSTOMER_RECORDS&userCard="+userCard,
            };

            var messageToReturn = new ResponseNewsMessage(msg)
            {
                CreateTime = DateTime.Now.Ticks,
                ArticleCount = 1,
                Articles = new List<ArticleMessage>(),
            };
            messageToReturn.Articles.Add(welcomeArticle);
            return messageToReturn;
        }

        public static ResponseMessage formSalerEntryNewsMsg(RequestMessage msg)
        {
            string saler_id = VerifyActions.verifySalerOpenId(msg.FromUserName);
            if (saler_id==null)
            {
                return new ResponseTextMessage(msg)
                {
                    CreateTime = DateTime.Now.Ticks,
                    Content = "当前微信用户并未成为任何商家的绑定微信，无法进入商家管理界面。若要成为商家绑定微信号"
                        + "请咨询管理人员，获得商家代码和验证码后，发送[绑定商家 商家代码 验证码]，例如[绑定商家 108 9090980]。注意不要打括号哦！"
                };
            }
            var welcomeArticle = new ArticleMessage()
            {
                Title = "网页版商家管理系统，一样强大！",
                PicUrl = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/Img/wechat_cover_salerEntry.jpg",
                Url = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/saler.html?openid=" + msg.FromUserName
                    + "&tick=" + Universal.getCurrentUnixTicks() + "&isViewOnly=false&func=FUNC_QUERY_SALER_RECORDS&salerId=" + saler_id,
            };

            var messageToReturn = new ResponseNewsMessage(msg)
            {
                CreateTime = DateTime.Now.Ticks,
                ArticleCount = 1,
                Articles = new List<ArticleMessage>(),
            };
            messageToReturn.Articles.Add(welcomeArticle);
            return messageToReturn;
        }

        public static ResponseMessage formAdminLoginNewsMsg(RequestMessage msg)
        {
            var welcomeArticle = new ArticleMessage()
            {
                Title = "你成功发现了管理后台的秘密！",
                PicUrl = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/Img/wechat_cover_welcome.png",
                Url = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/admin.html?openid=" + msg.FromUserName
                    + "&tick=" + Universal.getCurrentUnixTicks() + "&isViewOnly=false&func=FUNC_ADD_SALER",
            };

            var messageToReturn = new ResponseNewsMessage(msg)
            {
                CreateTime = DateTime.Now.Ticks,
                ArticleCount = 1,
                Articles = new List<ArticleMessage>(),
            };
            messageToReturn.Articles.Add(welcomeArticle);
            return messageToReturn;
        }

        public static ResponseMessage formAdminQueryNewsMsg(RequestMessage msg)
        {
            var welcomeArticle = new ArticleMessage()
            {
                Title = "看看你已经征服了多大的世界！",
                PicUrl = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/Img/wechat_cover_welcome.png",
                Url = System.Configuration.ConfigurationManager.AppSettings["serverPath"] + "WebApp/admin.html?openid=" + msg.FromUserName
                    + "&tick=" + Universal.getCurrentUnixTicks() + "&isViewOnly=false&func=FUNC_QUERY_ALL_SALER",
            };

            var messageToReturn = new ResponseNewsMessage(msg)
            {
                CreateTime = DateTime.Now.Ticks,
                ArticleCount = 1,
                Articles = new List<ArticleMessage>(),
            };
            messageToReturn.Articles.Add(welcomeArticle);
            return messageToReturn;
        }

    }
}