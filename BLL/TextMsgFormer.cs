﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using WX.Framework;
using WX.Model;
using WX.Api;
using WX.Model.ApiRequests;
using WeChatServe.Support;


namespace WeChatServe.BLL
{
    public static class TextMsgFormer
    {
        static TextMsgFormer()
        {

        }

        public static ResponseMessage formSubscribeTextMsg(RequestMessage msg)
        {
            return new ResponseTextMessage(msg)
            {
                CreateTime = DateTime.Now.Ticks,
                Content = System.Configuration.ConfigurationManager.AppSettings["subscribeInfo"]
            };
        }

        public static ResponseMessage formReleaseNewCardTextMsg(RequestMessage msg)
        {
            return new ResponseTextMessage(msg)
            {
                CreateTime = DateTime.Now.Ticks,
                Content = System.Configuration.ConfigurationManager.AppSettings["releaseNewCardInfo"]
            };
        }

        public static ResponseMessage formAddNewDealTextMsg(RequestMessage msg)
        {
            return new ResponseTextMessage(msg)
            {
                CreateTime = DateTime.Now.Ticks,
                Content = System.Configuration.ConfigurationManager.AppSettings["addNewDealInfo"]
            };
        }

        public static ResponseMessage formActivateInfoTextMsg(RequestMessage msg)
        {
            return new ResponseTextMessage(msg)
            {
                CreateTime = DateTime.Now.Ticks,
                Content = System.Configuration.ConfigurationManager.AppSettings["activateCardInfo"]
            };
        }

        public static ResponseMessage formNotSalerOpenIdTextMsg(RequestMessage msg)
        {
            return new ResponseTextMessage(msg)
            {
                CreateTime = DateTime.Now.Ticks,
                Content = "当前微信用户并未成为任何商家的绑定微信，无法进入商家管理界面。若要成为商家绑定微信号"
                    + "请咨询管理人员，获得商家代码和验证码后，发送[绑定商家 商家代码 验证码]，例如[绑定商家 108 9090980]。注意不要打括号哦！"
            };
        }
    }
}