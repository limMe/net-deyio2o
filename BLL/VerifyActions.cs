﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data;
using WeChatServe.Support;
using MySql.Data.MySqlClient;

namespace WeChatServe.BLL
{
    public class VerifyActions
    {
        public static bool verifyNonceToken(string nonceToken)
        {
            //DEBUG
            return true;

            string hour = DateTime.Now.ToString("HH");
            string minute = DateTime.Now.ToString("mm");

            string deleteSql = Universal.formDeleteOldToken(hour, minute);
            DatabaseManager.getDBManager().excuteNonQuery(deleteSql);

            string countSql = Universal.formWhetherTokenAvalible(nonceToken);
            if(DatabaseManager.getDBManager().excuteNumQuery(countSql)>0)
            {
                return true;
            }
            return false;
        }

        public static bool verifyAdminOpenId(string openId)
        {
            string countSql = Universal.formWhetherAdminIdRight(openId);
            if (DatabaseManager.getDBManager().excuteNumQuery(countSql) > 0)
            {
                return true;
            }
            return false;
        }

        public static string getNonceToken()
        {
            string hour = DateTime.Now.ToString("HH");
            string minute = DateTime.Now.ToString("mm");

            string deleteSql = Universal.formDeleteOldToken(hour, minute);
            DatabaseManager.getDBManager().excuteNonQuery(deleteSql);

            string querySql = Universal.formGetNonceToken(hour, minute);
            var reader = DatabaseManager.getDBManager().excuteQuery(querySql);
            if(reader.Read())
            {
                if(reader[0].ToString() != null)
                {
                    reader.Close();
                    return reader[0].ToString();
                }
            }
            reader.Close();

            Random rm = new Random();
            string newToken = rm.Next(100000, 999999).ToString();
            string insertSql = Universal.formInsertNewToken(hour, minute, newToken);
            DatabaseManager.getDBManager().excuteNonQuery(insertSql);
            return newToken;
        }

        public static string getUserCardByOpenId(string open_id)
        {
            string quertStr = Universal.formGetCardNumByOpenId(open_id);
            MySql.Data.MySqlClient.MySqlDataReader reader = DatabaseManager.getDBManager().excuteQuery(quertStr);
            string cardnum = null;
            if (reader.Read())
            {
                cardnum = reader[0].ToString();
            }
            reader.Close();
            if (cardnum != null)
                return cardnum;
            else
                return null;

        }

        public static string verifySalerOpenId(string openid)
        {
            string sql = Universal.formGetSalerIdByOpenId(openid);
            MySqlDataReader reader = DatabaseManager.getDBManager().excuteQuery(sql);
            string saler_id = null;
            if(reader.Read())
            {
                saler_id = reader[0].ToString();
            }
            reader.Close();
            return saler_id;
        }
    }
}