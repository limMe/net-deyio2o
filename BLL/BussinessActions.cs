﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using MySql.Data.MySqlClient;
using WeChatServe.Support;
using WeChatServe.InteractModel;
using Newtonsoft.Json;

namespace WeChatServe.BLL
{
    /// <summary>
    /// 业务逻辑层好多具体的任务都是在这里实现的
    /// 记得写入数据时需要验证数据是否正确
    /// </summary>
    public static class BussinessActions
    {
        /// <summary>
        /// 发新卡，目前来说，只支持商家发新卡
        /// </summary>
        /// <param name="currentRequest"></param>
        /// <returns></returns>
        public static string publishNewCard(NewCardModel newCard)
        {
            if(newCard.customerCard == 0 || newCard.salerId == 0)
            {
                throw new Exception("缺少必要的新卡信息");
            }

            string queryStr1 = Universal.formWhetherCardExists(newCard.customerCard.ToString());
            int? isExits = DatabaseManager.getDBManager().excuteNumQuery(queryStr1);

            string queryStr2 = Universal.formWhetherCardNeedVerify(newCard.customerCard.ToString());
            int? needVerify = DatabaseManager.getDBManager().excuteNumQuery(queryStr2);

            if (isExits == 0 && needVerify == 0)
            {
                Random randomSeed = new Random();
                int verifyNum = randomSeed.Next(1000, 9999);
                string insertSql = Universal.formInsertUnverifiedCard(newCard.customerCard.ToString()
                    , newCard.salerId.ToString(), verifyNum.ToString(), DateTime.Now.ToString("yyyyMMdd"));
                DatabaseManager.getDBManager().excuteNonQuery(insertSql);
                AddCardResultModel result = new AddCardResultModel();
                result.status = true;
                result.verifyNum = verifyNum;
                return(JsonConvert.SerializeObject(result));
            }
            else if (isExits == 1)
            {
                throw new Exception("该卡号已经被发行，请勿重新发卡！");
            }
            else if (needVerify == 1)
            {
                string queryVerifyStr = Universal.formGetVerifyNumQuery(newCard.customerCard.ToString());
                MySql.Data.MySqlClient.MySqlDataReader reader = DatabaseManager.getDBManager().excuteQuery(queryVerifyStr);
                reader.Read();
                string errorMsg = "您已经发行过这张卡片，请不要在用户验证卡片前再次发行这张卡片。这张卡的校验码是:" + reader[0];
                reader.Close();
                throw new Exception(errorMsg);
            }
            else
                throw new Exception("该卡号有问题，请换卡重试。");
        }
        /// <summary>
        /// 新增交易，交易的时间是这里决定的
        /// </summary>
        /// <param name="currentRequest"></param>
        public static string addNewDeal(NewDealModel dealToAdd)
        {
            if(dealToAdd.customerCard == 0
                || dealToAdd.salerId == 0)
            {
                throw new Exception("缺少必要的交易信息");
            }

            if(dealToAdd.money100 <= 0)
            {
                throw new Exception("交易金额必须是正数!");
            }

            if(dealToAdd.money100 > 500000)
            {
                throw new Exception("目前交易金额不能超过5000元！");
            }

            string queryStr = Universal.formGetUserInfoQuery(dealToAdd.customerCard.ToString());
            MySql.Data.MySqlClient.MySqlDataReader reader = DatabaseManager.getDBManager().excuteQuery(queryStr);
            string bundle_id;
            if (reader.Read())
            {
                bundle_id = reader[1].ToString();
            }
            else
            {
                reader.Close();
                throw new Exception("该卡号尚未激活");
            }
            reader.Close();

            string sqlStr = Universal.formInsertNewDeal(dealToAdd.customerCard.ToString(), dealToAdd.salerId.ToString(), 
                    dealToAdd.money100.ToString(), DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.ToString("HHmmss"));
            DatabaseManager.getDBManager().excuteNonQuery(sqlStr);
            
            string updateStr = Universal.formUpdateUserPoints(dealToAdd.customerCard.ToString(), dealToAdd.money100.ToString());
            DatabaseManager.getDBManager().excuteNonQuery(updateStr);
            
            queryStr = Universal.formGetSalerInfoQuery(dealToAdd.salerId.ToString());
            MySql.Data.MySqlClient.MySqlDataReader reader2 = DatabaseManager.getDBManager().excuteQuery(queryStr);
            string salerName;
            if (reader2.Read())
            {
                salerName = reader2[0].ToString();
            }
            else
            {
                throw new Exception("您的商家配置文件有问题");
            }
            reader2.Close();
            TemplateMsgFormer.sendDealMadeTempMsg(bundle_id, salerName, dealToAdd.money100);
            SingleDataModel sd = new SingleDataModel();
            sd.data = "添加成功";
            sd.status = true;
            return JsonConvert.SerializeObject(sd);
        }

        public static string queryDealRecords(QueryRecordsModel queryToDo, bool isSaler = true)
        {
            
            if(queryToDo.beginDate == null
                || queryToDo.endDate == null)
            {
                throw new Exception("缺少必要的查询信息");
            }

            string sqlStr;
            if (isSaler)
            {
                sqlStr = Universal.formSalerRecordsQuery(queryToDo.salerId.ToString(), queryToDo.beginDate.ToString(),
                        queryToDo.endDate.ToString());
            }
            else
            {
                sqlStr = Universal.formCustomerRecordsQuery(queryToDo.user_card.ToString(), queryToDo.beginDate.ToString(),
                        queryToDo.endDate.ToString());
            }
            MySql.Data.MySqlClient.MySqlDataReader reader = DatabaseManager.getDBManager().excuteQuery(sqlStr);
            RecordsResultModel result = new RecordsResultModel();
            result.recordsList = new List<RecordItem>();
            while(reader.Read())
            {
                RecordItem newItem = new RecordItem();
                
                newItem.dealId = Int64.Parse(reader[0].ToString());
                newItem.salerId = Int64.Parse(reader[1].ToString());
                newItem.userCard = Int64.Parse(reader[2].ToString());
                newItem.amount = Int32.Parse(reader[3].ToString());
                newItem.date = reader[4].ToString();
                newItem.time = reader[5].ToString();
                
                result.recordsList.Add(newItem);
                
            }
            
            reader.Close();

            result.status = true;
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 验证新卡，由于从微信上调用，这里不能带request
        /// </summary>
        /// <param name="cardNum"></param>
        /// <param name="verifyNum"></param>
        /// <param name="open_id"></param>
        /// <returns></returns>
        public static string verifyCard(ActiveCardModel activeToDo)
        {
            int cardNum = activeToDo.cardId,
                verifyNum = activeToDo.activecode;
            string open_id = activeToDo.openid;
            if(cardNum == 0|| verifyNum == 0)
            {
                throw new Exception("缺少卡片激活信息");
            }

            if(cardNum > Universal.maxCardNum
                || cardNum < 0
                || verifyNum < 1000
                || verifyNum > 9999)
            {
                throw new Exception("卡号或验证码无效。");
            }

            string checkStr = Universal.formWhetherWechatHasCard(open_id);
            if(DatabaseManager.getDBManager().excuteNumQuery(checkStr)>0)
            {
                throw new Exception("您的微信号已经绑定过一张卡了，不能再绑定新的卡片。");
            }

            string sqlStr = Universal.formGetVerifyInfoQuery(cardNum.ToString());
            var reader = DatabaseManager.getDBManager().excuteQuery(sqlStr);
            if(reader.Read())
            {
                if (reader[0] != null)
                {
                    if (reader[0].ToString() == verifyNum.ToString())
                    {
                        string intro_saler = reader[1].ToString();
                        string intro_user = reader[2].ToString();
                        reader.Close();

                        if(intro_saler == null || intro_saler == "")
                        {
                            intro_saler = "-1";
                        }

                        if(intro_user == null || intro_user == "")
                        {
                            intro_user = "-1";
                        }

                        string insertSql = Universal.formInsertNewCardUser(cardNum.ToString(), open_id,intro_saler,intro_user);
                        //throw new Exception(insertSql);
                        DatabaseManager.getDBManager().excuteNonQuery(insertSql);

                        string deleteSql = Universal.formDeleteCardNeedVerify(cardNum.ToString());
                        DatabaseManager.getDBManager().excuteNonQuery(deleteSql);

                        SingleDataModel sd = new SingleDataModel();
                        sd.data = "激活成功";
                        sd.status = true;
                        return JsonConvert.SerializeObject(sd);
                    }
                    else
                    {
                        reader.Close();
                        throw new Exception("验证码错误");
                    }
                }
            }
            reader.Close();
            throw new Exception("这是一张还未发行或者已经被激活了的的卡片");
        }

        public static string addSaler(string salerName, string salerLocation, string salerType, string salerDistrict)
        {
            if (salerDistrict == null || salerType==null || salerName == null)
            {
                throw new Exception("信息不完整");
            }
            if(!Regex.IsMatch(salerDistrict, @"^-?\d+$"))
            {
                throw new Exception("商户商区应当是个整数代码");
            }
            if(!Regex.IsMatch(salerType, @"^-?\d+$"))
            {
                throw new Exception("商户类型应当是个整数代码");
            }
            Random rd = new Random();
            string activate_code = rd.Next(100000, 999999).ToString();
            string insertSql = Universal.formInsertNewSaler(salerName, salerLocation, salerDistrict, salerType, activate_code);
            DatabaseManager.getDBManager().excuteNonQuery(insertSql);
            SingleDataModel sd = new SingleDataModel();
            sd.data = activate_code;
            sd.status = true;
            return JsonConvert.SerializeObject(sd);
        }

        public static string querySalerInfoByName(string salerName)
        {
            List<SalerInfoModel> salersToReturn = new List<SalerInfoModel>();
            throw new NotImplementedException();
        }

        public static string querySalerInfoById(string saler_id)
        {
            string sqlStr = Universal.formGetFullSalerInfoQuery(saler_id);
            var reader = DatabaseManager.getDBManager().excuteQuery(sqlStr);
            var salerInfoToReturn = new SalerInfoModel();
            if(reader.Read())
            {
                salerInfoToReturn.saler_id = int.Parse(reader[0].ToString());
                salerInfoToReturn.saler_name = reader[1].ToString();
                salerInfoToReturn.saler_open_id = reader[2].ToString();
                salerInfoToReturn.saler_location = reader[3].ToString();
                int district = 0;
                int.TryParse(reader[4].ToString(), out district);
                salerInfoToReturn.saler_district = district;
                int type = 0;
                int.TryParse(reader[5].ToString(), out type);
                salerInfoToReturn.saler_type = type;
                if (reader[6].ToString() == null)
                {
                    throw new Exception("该商家信息有错误。");
                }
                salerInfoToReturn.saler_activate_code = int.Parse(reader[6].ToString());
                salerInfoToReturn.status = true;
            }
            reader.Close();
            return JsonConvert.SerializeObject(salerInfoToReturn);
        }

        public static string loginSalerInfo(string saler_id, string saler_activate_code)
        {
            string sqlStr = Universal.formGetFullSalerInfoQuery(saler_id);
            var reader = DatabaseManager.getDBManager().excuteQuery(sqlStr);
            var salerInfoToReturn = new SalerInfoModel();
            if (reader.Read())
            {
                salerInfoToReturn.saler_id = int.Parse(reader[0].ToString());
                salerInfoToReturn.saler_name = reader[1].ToString();
                salerInfoToReturn.saler_open_id = reader[2].ToString();
                salerInfoToReturn.saler_location = reader[3].ToString();
                int district = 0;
                int.TryParse(reader[4].ToString(), out district);
                salerInfoToReturn.saler_district = district;
                int type = 0;
                int.TryParse(reader[5].ToString(), out type);
                salerInfoToReturn.saler_type = type;
                if (reader[6].ToString() == null)
                {
                    throw new Exception("该商家信息有错误。");
                }
                if (!reader[6].ToString().Equals(saler_activate_code))
                    throw new Exception("密码错误，请核对");
                salerInfoToReturn.saler_activate_code = int.Parse(reader[6].ToString());
                salerInfoToReturn.status = true;
            }
            reader.Close();
            return JsonConvert.SerializeObject(salerInfoToReturn);
        }

        public static string queryAllSalerInfo()
        {
            string sqlStr = Universal.formGetAllSalerInfoQuery();
            var reader = DatabaseManager.getDBManager().excuteQuery(sqlStr);
            var dataToReturn = new ListDataModel();
            dataToReturn.data = new List<object>();
            while(reader.Read())
            {
                var salerInfoToReturn = new SalerInfoModel();
                salerInfoToReturn.saler_id = int.Parse(reader[0].ToString());
                salerInfoToReturn.saler_name = reader[1].ToString();
                salerInfoToReturn.saler_open_id = reader[2].ToString();
                salerInfoToReturn.saler_location = reader[3].ToString();
                int district = 0;
                int.TryParse(reader[4].ToString(), out district);
                salerInfoToReturn.saler_district = district;
                int type = 0;
                int.TryParse(reader[5].ToString(), out type);
                salerInfoToReturn.saler_type = type;
                if(reader[6].ToString() == null)
                {
                    reader.Close();
                    throw new Exception("该商家信息有错误。");
                }
                salerInfoToReturn.saler_activate_code = int.Parse(reader[6].ToString());
                dataToReturn.data.Add(salerInfoToReturn);
            }
            reader.Close();
            dataToReturn.status = true;
            return JsonConvert.SerializeObject(dataToReturn);
        }

        public static bool bandleSalerWechat(string saler_id, string verifyNum, string open_id)
        {
            string fullQuerySql = Universal.formGetFullSalerInfoQuery(saler_id);
            MySqlDataReader reader = DatabaseManager.getDBManager().excuteQuery(fullQuerySql);
            string oldOpenId = null;
            string activiteCode = null;
            if (reader.Read())
            {
                oldOpenId = reader[2].ToString();
                activiteCode = reader[6].ToString();
            }
            else
            {
                throw new Exception("商家代码不存在！");
            }
            reader.Close();

            if(activiteCode != verifyNum)
            {
                throw new Exception("验证码不正确！");
            }

            string[] openIds = oldOpenId.Split('&');
            if(openIds.Length >= 5)
            {
                throw new Exception("已经有5个微信号绑定了这个商户，不能再继续绑定了。");
            }
            for(int i=0; i<openIds.Length;i++)
            {
                if(openIds[i] == open_id)
                {
                    //M.S 有可能我一个微信号绑定了两家的商户。
                    throw new Exception("这个微信号已经绑定过该商户了。");
                }
            }

            string newOpenId = null;
            if(oldOpenId == null || oldOpenId =="")
            {
                newOpenId = open_id;
            }
            else
            {
                newOpenId = oldOpenId + "&" + open_id;
            }

            string insertSql = Universal.formUpdateSalerOpenId(saler_id, newOpenId);
            DatabaseManager.getDBManager().excuteNonQuery(insertSql);

            return true;
        }

        public static string deleteWrongDeal(HttpRequest currentRequest)
        {
            throw new NotImplementedException();
        }
    }
}