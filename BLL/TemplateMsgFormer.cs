﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using WX.Framework;
using WX.Model;
using WX.Api;
using WX.Model.ApiRequests;
using WX.Model.ApiResponses;
using WeChatServe.Support;
using System.Globalization;
using System.Threading;

namespace WeChatServe.BLL
{
    public static class TemplateMsgFormer
    {
        static TemplateMsgFormer()
        {

        }

        /// <summary>
        /// 发送一个消费提醒的模板消息,time会自动填充为当前服务器时间
        /// </summary>
        /// <param name="toUserOpenId">接收消息的用户open_id</param>
        /// <param name="address">发生交易的商家地点</param>
        /// <param name="money100">倍乘100消去小数的金额</param>
        /// <returns>是否成功</returns>
        public static bool sendDealMadeTempMsg(string toUserOpenId, string place, int money100)
        {
            var templateRequest = new TemplateSendRequest()
            {
                AccessToken = ApiAccessTokenManager.Instance.GetCurrentToken(),
                ToUser = toUserOpenId,
                TemplateID = System.Configuration.ConfigurationManager.AppSettings["dealMadeTemplate"],
                Url = System.Configuration.ConfigurationManager.AppSettings["dealMadeTemplateUrlHead"],
                TopColor = System.Configuration.ConfigurationManager.AppSettings["#FF0000"],
                Data = new Dictionary<string, TemplateDataProperty>(),
            };
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("zh-CN");
            templateRequest.Data.Add("time", new TemplateDataProperty(DateTime.Now.ToString(),"#173177"));
            templateRequest.Data.Add("remark", new TemplateDataProperty(System.Configuration.ConfigurationManager.AppSettings["dealMadeTemplateInfo"], "#173177"));
            templateRequest.Data.Add("pay", new TemplateDataProperty(Universal.getDisplayableMoney(money100), "#173177"));
            templateRequest.Data.Add("address", new TemplateDataProperty(place, "#173177"));

            IApiClient pushTempClient = new DefaultApiClient();
            TemplateSendResponse templateResponse = pushTempClient.Execute(templateRequest);

            return templateResponse.IsError;
        }
    }
}