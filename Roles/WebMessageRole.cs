﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using WeChatServe.Handlers;
using WX.Framework;
using WX.Model;

namespace WeChatServe.Roles
{
    /// <summary>
    /// M.U
    /// 本类实际上比其他MessageHandler更加高级，Default.aspx将请求文本不经处理直接拿过来
    /// 重构时可以考虑把这里的功能拿到Default.aspx层级
    /// </summary>
    public class WebMessageRole : IMessageRole
    {
        public IMessageHandler MessageRole(MiddleMessage msg)
        {
            try
            {
                return new MsgTypeMessageRole().MessageRole(msg);
            }
            catch(Exception ex)
            {
                return new TextMessageHandler(ex.Message);
            }
        }
    }
}