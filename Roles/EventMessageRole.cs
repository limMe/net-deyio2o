﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using WX.Framework;
using WX.Model;
using WeChatServe.Handlers;

namespace WeChatServe.Roles
{
    /// <summary>
    /// 对用户关注、取消关注、扫描、地理位置上传等事件做出的相应。
    /// </summary>
    public class EventMessageRole : IMessageRole
    {
        public IMessageHandler MessageRole(MiddleMessage msg)
        {
            var eventType = (Event)Enum.Parse(typeof(Event), msg.Element.Element("Event").Value, true);

            switch (eventType)
            {
                case Event.Subscribe:
                    return new SubScribeEventMessageHandler(msg);
                
                case Event.Unsubscribe:
                    return new UnSubScribeEventMessageHandler(msg);
                
                case Event.Click:
                    return new ClickMyMenuEventMessageHandler(msg);
            }

            return new DefaultMessageHandler();
        }
    }
}