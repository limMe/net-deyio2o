﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using WeChatServe.Handlers;
using WX.Framework;
using WX.Model;

namespace WeChatServe.Roles
{
    /// <summary>
    /// M.U
    /// 本类实际上比其他MessageHandler更加高级，Default.aspx将请求文本经过Web拿过来
    /// 重构时可以考虑把这里的功能拿到Default.aspx层级
    /// </summary>
    public class MsgTypeMessageRole : IMessageRole
    {
        public IMessageHandler MessageRole(MiddleMessage msg)
        {
            switch (msg.RequestMessage.MsgType)
            {
                
                case MsgType.Text:
                    return new TextMessageRole().MessageRole(msg);

                
                case MsgType.Event:
                    return new EventMessageRole().MessageRole(msg);

                /*
                case MsgType.Voice:
                    return new VoiceMessageRole().MessageRole(msg);
                 */
                default:
                    return new DefaultMessageHandler();
            }
        }
    }
}