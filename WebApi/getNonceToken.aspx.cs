﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WeChatServe.WebApi
{
    public partial class getNonceToken : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var dataToReturn = new InteractModel.SingleDataModel();
                dataToReturn.data = BLL.VerifyActions.getNonceToken();
                dataToReturn.status = true;
                Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(dataToReturn));
            }
            catch(Exception ex)
            {
                var rm = new InteractModel.RootModel();
                rm.status = false;
                rm.errorMsg = ex.Message;
                Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(rm));
            }
        }
    }
}