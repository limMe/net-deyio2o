﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WeChatServe.BLL;
using WeChatServe.InteractModel;


namespace WeChatServe.WebApi
{
    public partial class loginSaler : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string saler_id = Request.QueryString["saler_id"];
                string saler_activate_code = Request.QueryString["saler_activate_code"]; ;
                Response.Write(BussinessActions.loginSalerInfo(saler_id, saler_activate_code));
            }
            catch (Exception ex)
            {
                RootModel rm = new RootModel();
                rm.status = false;
                rm.errorMsg = ex.Message;
                Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(rm));
            }
        }
    }
}