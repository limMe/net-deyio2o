﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WeChatServe.BLL;
using WeChatServe.InteractModel;
using WeChatServe.Support;

namespace WeChatServe.WebApi
{
    public partial class addNewDeal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                NewDealModel dealToAdd = new NewDealModel();
                dealToAdd.salerId = int.Parse(Request.QueryString["saler_id"]);
                dealToAdd.customerCard = int.Parse(Request.QueryString["customer_id"]);
                dealToAdd.money100 = (int)Universal.toMoney100(Request.QueryString["customer_cost"]);
                Response.Write(BussinessActions.addNewDeal(dealToAdd));
            }
            catch (Exception ex)
            {
                RootModel rm = new RootModel();
                rm.status = false;
                rm.errorMsg = ex.Message;
                Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(rm));
            }
        }
    }
}