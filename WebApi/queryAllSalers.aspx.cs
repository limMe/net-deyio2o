﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WeChatServe.BLL;
using WeChatServe.InteractModel;

namespace WeChatServe.WebApi
{
    public partial class queryAllSalers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                /*
                string nonce_token = Request.QueryString["nonceToken"];
                if (!VerifyActions.verifyNonceToken(nonce_token))
                {
                    throw new Exception("登录已超时。请在公众号中重新打开该功能模块再试。");
                }*/
                Response.Write(BussinessActions.queryAllSalerInfo());
            }
            catch (Exception ex)
            {
                RootModel rm = new RootModel();
                rm.status = false;
                rm.errorMsg = ex.Message;
                Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(rm));
            }
        }
    }
}