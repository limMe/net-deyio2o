﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WeChatServe.BLL;
using WeChatServe.InteractModel;

namespace WeChatServe.WebApi
{
    public partial class querySalerRecords : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                QueryRecordsModel queryToDo = new QueryRecordsModel();
                queryToDo.beginDate = Request.QueryString["beginDate"];
                queryToDo.endDate = Request.QueryString["endDate"];
                queryToDo.salerId = int.Parse(Request.QueryString["salerId"]);

                Response.Write(BussinessActions.queryDealRecords(queryToDo));
            }
            catch(Exception ex)
            {
                RootModel rm = new RootModel();
                rm.status = false;
                rm.errorMsg = ex.Message;
                Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(rm));
            }
            
        }
    }
}