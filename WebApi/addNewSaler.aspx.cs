﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WeChatServe.BLL;
using WeChatServe.InteractModel;

namespace WeChatServe.WebApi
{
    /// <summary>
    /// M.S 
    /// </summary>
    public partial class addNewSaler : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string nonce_token = Request.Params["nonceToken"];
                if (!VerifyActions.verifyNonceToken(nonce_token))
                {
                    throw new Exception("登录已超时。请在公众号中重新打开该功能模块再试。");
                }

                string openId = Request.Params["openId"];
                if(!VerifyActions.verifyAdminOpenId(openId))
                {
                    throw new Exception("您不是系统管理员。");
                }

                string salerName = Request.Params["salerName"];
                string salerLocation = Request.Params["salerLocation"];
                string salerType = Request.Params["salerType"];
                string salerDistrict = Request.Params["salerDistrict"];
                Response.Write(BussinessActions.addSaler(salerName,salerLocation,salerType,salerDistrict));                
            }
            catch (Exception ex)
            {
                RootModel rm = new RootModel();
                rm.status = false;
                rm.errorMsg = ex.Message;
                Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(rm));
            }
        }
    }
}