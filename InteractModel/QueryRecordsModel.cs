﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeChatServe.InteractModel
{
    public class QueryRecordsModel : RootModel
    {
        public int user_card { get; set; }
        public int salerId { get; set; }
        public string beginDate { get; set; }
        public string endDate { get; set; }
    }
}
