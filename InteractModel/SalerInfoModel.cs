﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeChatServe.InteractModel
{
    public class SalerInfoModel : RootModel
    {
        public int saler_id { get; set; }
        public string saler_name { get; set; }
        public string saler_open_id { get; set; }
        public string saler_location { get; set; }
        public int saler_district { get; set; }
        public int saler_type { get; set; }
        public int saler_activate_code { get; set; }
    }

    public enum SalerDistrict
    {
        MultiDistrict = 0,
        RenminUniversity = 1,
        Weigongcun = 2,
    }

    public enum Saler_type
    {
        MultiType = 0,
        Restaurant = 1,
        Hotel = 2,
        Retail = 3,
        Health = 4,
        Beauty = 5,
    }
}