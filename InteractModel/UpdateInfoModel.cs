﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeChatServe.InteractModel
{
    /// <summary>
    /// 只会让客户端下载一个exe,下载后让这个exe执行
    /// </summary>
    public class UpdateInfoModel : RootModel
    {
        public int currentBigVersion { get; set; }
        public string fileName { get; set; }
        public string fileUrl { get; set; }
    }
}