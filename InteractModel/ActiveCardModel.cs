﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeChatServe.InteractModel
{
    public class ActiveCardModel : RootModel
    {
        public int cardId { get; set; }
        public int activecode { get; set; }
        public string openid{ get; set; }
    }
}
