﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using WeChatServe.BLL;
using WeChatServe.InteractModel;

namespace WeChatServe.WinApi
{
    public partial class checkUpdate : System.Web.UI.Page
    {
        //E.D
        private static string updateDictionary = "update/";
        private static string updateInfo = "latest_version.txt";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                FileStream updateInfoStream = new FileStream(HttpContext.Current.Server.MapPath(updateDictionary + updateInfo), FileMode.Open);
                StreamReader updateInfoReader = new StreamReader(updateInfoStream, Encoding.UTF8);
                var updateInfoToSend = new UpdateInfoModel();
                updateInfoToSend.currentBigVersion = int.Parse(updateInfoReader.ReadLine());
                updateInfoToSend.fileName = updateInfoReader.ReadLine();
                updateInfoToSend.fileUrl = "http://" +Request.Url.Authority + "/WinApi/" + updateDictionary + updateInfoToSend.fileName;
                updateInfoToSend.status = true;
                updateInfoReader.Close();
                updateInfoStream.Close();
                Response.Write(JsonConvert.SerializeObject(updateInfoToSend));
            }
            catch (Exception ex)
            {
                RootModel result = new RootModel();
                result.status = false;
                result.errorMsg = ex.Message;
                Response.Write(JsonConvert.SerializeObject(result));
            }
        }
    }
}