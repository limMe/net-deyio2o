﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WeChatServe.BLL;
using WeChatServe.InteractModel;
using Newtonsoft.Json;

namespace WeChatServe.WinApi
{
    public partial class querySalerRecords : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //HttpRequest currentRequest = Request;
                if (!Support.Universal.verifyEncrpt(Request.QueryString["rKey"], Request.QueryString["eKey"]))
                {
                    throw new Exception("来自不明身份的客户端。");
                }
                var data = Request.QueryString["data"];
                QueryRecordsModel queryToDo = JsonConvert.DeserializeObject<QueryRecordsModel>(data);

                Response.Write(BussinessActions.queryDealRecords(queryToDo));

            }
            catch (Exception ex)
            {
                RecordsResultModel result = new RecordsResultModel();
                result.status = false;
                result.errorMsg = ex.Message;
                Response.Write(JsonConvert.SerializeObject(result));
            }

        }
    }
}