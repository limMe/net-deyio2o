﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WeChatServe.BLL;
using WeChatServe.InteractModel;
using Newtonsoft.Json;

namespace WeChatServe.WinApi
{
    public partial class addNewDeal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Support.Universal.verifyEncrpt(Request.QueryString["rKey"], Request.QueryString["eKey"]))
                {
                    throw new Exception("来自不明身份的客户端。");
                }
                var data = Request.QueryString["data"];
                NewDealModel dealToAdd = JsonConvert.DeserializeObject<NewDealModel>(data);
                BussinessActions.addNewDeal(dealToAdd);
                RootModel result = new RootModel();
                result.status = true;
                Response.Write(JsonConvert.SerializeObject(result));
            }
            catch (Exception ex)
            {
                RootModel result = new RootModel();
                result.status = false;
                result.errorMsg = ex.Message;
                Response.Write(JsonConvert.SerializeObject(result));
            }
        }
    }
}